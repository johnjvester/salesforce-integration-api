package com.gitlab.johnjvester.salesforceintegration.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(schema = "salesforce", name="timeentry__c")
public class Time {
    @Id
    private long id;

    @Column(name = "date__c")
    private Date date;

    @Column(name = "hours__c")
    private float hours;
}

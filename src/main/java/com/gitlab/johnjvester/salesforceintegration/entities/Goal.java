package com.gitlab.johnjvester.salesforceintegration.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(schema = "public", name="GOALS")
public class Goal {
    @Id
    private int id;

    @Column(name = "GOAL_MONTH")
    private int month;

    @Column(name = "GOAL_YEAR")
    private int year;

    @Column(name = "GOAL_TARGET")
    private int goal;

    @Column(name = "BILLABLE_DAYS")
    private int days;

    @Transient
    public float getAveragePerDay() {
        if (goal > 0 && days > 0) {
            return (float) goal /  (float) days;
        }

        return 0f;
    }

}

package com.gitlab.johnjvester.salesforceintegration.utils;

import com.gitlab.johnjvester.salesforceintegration.entities.Time;
import com.gitlab.johnjvester.salesforceintegration.models.Month;
import org.apache.commons.collections4.CollectionUtils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class TimeUtils {
    private TimeUtils() { }

    public static List<Month> getMonths(List<Time> timeList) {
        Set<Month> months = new HashSet<>();

        if (CollectionUtils.isNotEmpty(timeList)) {
            for (Time time: timeList) {
                LocalDate localDate = time.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                Month month = new Month();
                month.setMonth(localDate.getMonthValue());
                month.setYear(localDate.getYear());
                months.add(month);
            }
        }

        return months.stream()
                .sorted(Comparator.comparing(Month::getYearMonth))
                .collect(Collectors.toList());
    }
}

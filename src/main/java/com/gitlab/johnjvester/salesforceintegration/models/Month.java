package com.gitlab.johnjvester.salesforceintegration.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Month {
    private int month;
    private int year;

    public String getDisplayName() {
        return month + "/" + year;
    }

    public String getYearMonth() {
        return year + "" + month;
    }
}

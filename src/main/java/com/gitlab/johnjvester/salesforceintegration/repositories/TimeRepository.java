package com.gitlab.johnjvester.salesforceintegration.repositories;

import com.gitlab.johnjvester.salesforceintegration.entities.Time;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface TimeRepository extends JpaRepository<Time, Long> {
    List<Time> findAllByDateGreaterThanEqualAndDateLessThanEqualOrderByDateAsc(Date startDate, Date endDate);
}

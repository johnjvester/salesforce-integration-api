package com.gitlab.johnjvester.salesforceintegration.repositories;

import com.gitlab.johnjvester.salesforceintegration.entities.Goal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoalRepository extends JpaRepository<Goal, Integer> {
    Goal findByMonthEqualsAndYearEquals(int month, int year);
}

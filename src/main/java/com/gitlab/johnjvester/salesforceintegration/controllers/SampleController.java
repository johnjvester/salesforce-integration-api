package com.gitlab.johnjvester.salesforceintegration.controllers;

import com.gitlab.johnjvester.salesforceintegration.entities.Goal;
import com.gitlab.johnjvester.salesforceintegration.entities.Time;
import com.gitlab.johnjvester.salesforceintegration.models.Month;
import com.gitlab.johnjvester.salesforceintegration.repositories.GoalRepository;
import com.gitlab.johnjvester.salesforceintegration.repositories.TimeRepository;
import com.gitlab.johnjvester.salesforceintegration.utils.TimeUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@CrossOrigin
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class SampleController {
    private final GoalRepository goalRepository;
    private final TimeRepository timeRepository;

    @GetMapping(value = "/goal/{year}/{month}")
    public ResponseEntity<Goal> getGoal(@PathVariable int year, @PathVariable int month) {
        log.debug("getGoal(year={}, month={})", year, month);
        return new ResponseEntity<>(goalRepository.findByMonthEqualsAndYearEquals(month, year), HttpStatus.OK);
    }

    @GetMapping(value = "/time/{year}/{month}")
    public ResponseEntity<List<Time>> getTime(@PathVariable int year, @PathVariable int month) {
        log.debug("getTime(year={}, month={})", year, month);

        LocalDate startDate = LocalDate.parse(year + "-" + month + "-01");
        LocalDate endDate = startDate.plusMonths(1).minusDays(1);
        log.debug("startDate={}, endDate={}", startDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")), endDate.format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));

        return new ResponseEntity<>(timeRepository.findAllByDateGreaterThanEqualAndDateLessThanEqualOrderByDateAsc(Date.from(startDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()), Date.from(endDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())), HttpStatus.OK);
    }

    @GetMapping(value = "/months")
    public ResponseEntity<List<Month>> getMonths() {
        log.debug("getMonths()");
        return new ResponseEntity<>(TimeUtils.getMonths(timeRepository.findAll(Sort.by(Sort.Direction.ASC, "date"))), HttpStatus.OK);
    }
}
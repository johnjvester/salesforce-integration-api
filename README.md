# Salesforce Integration Application (`salesforce-integration-api`)

[![pipeline status](https://gitlab.com/johnjvester/salesforce-integration-api/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/salesforce-integration-api/commits/master)

> The Salesforce Integration Spring Boot Example Application (`salesforce-integration-api`) is a 
> [Java](<https://en.wikipedia.org/wiki/Java_(programming_language)>) based 
> application, utilizing the [Spring Boot](<https://spring.io/projects/spring-boot>) framework and is intended to 
> provide a working example of integrating [Salesforce](https://www.salesforce.com/) data into a 
> [Heroku PostgreSQL](https://elements.heroku.com/addons/heroku-postgresql) database using the
> [Heroku Connect](https://elements.heroku.com/addons/herokuconnect) 
> middleware solution.  When used with the [`salesforce-integration-client`](<https://gitlab.com/johnjvester/salesforce-integration-client>) repository, a full-stack solution can be
> experienced.

![Angular Client Example](./AngularClientExample.png)

## Publications

This repository is related to an article published on DZone.com:

* [Integrating Traditional Cloud Development with Salesforce](https://dzone.com/articles/integrating-traditional-cloud-development-with-sal)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Configuration (Spring Boot API)

After adding Heroku PostgreSQL to a Heroku dyno, the following properties are expected:

The following elements need to be set within Spring Boot in order to run the `salesforce-integration-api` application:

* `${JDBC_DATABASE_URL}` - database URL to access PostgreSQL `[spring.datasource.url]`
* `${JDBC_DATABASE_USERNAME}` - user name to access the PostgreSQL database `[spring.datasource.username]`
* `${JDBC_DATABASE_PASSWORD}` - password for the PostgresSQL database `[spring.datasource.password]`
* `${PORT}` - port for Spring Boot service `[server.port]` (optional, default is 8080)

Using a Heroku Dyno, the `JDBC_*` environment settings are already in place, as well as the `PORT` too.  Meaning, 
if you use this repository with Heroku, no changes are required from a Config Vars perspective

## Salesforce Configuration and Data

For this example a custom Salesforce object, called `TimeEntry` (`TimeEntry__c`) was created and contained two basic custom fields:

* `Date` (`Date__c`) - required Date field to store the date of the time entry
* `Hours` (`Hours__c`) - required Number(2,2) field to store the hours for given date

Once configured, the `TimeEntry` object can be used in the Salesforce UI to populate at least one full month of data. Example
date is provided below:

![Sample Salesforce Data](./TimeEntryData.png)

## Heroku PostgreSQL Configuration and Data

Within Heroku, a new Dyno can be created for use by this repository.  Before adding the Spring Boot source code, a Heroku Postgres add-on 
needs to be added.  For the purposes of this repository, the Hobby Dev edition will suffice.

One additional table, called `GOALS` needs to be added to the `public` schema:

```
CREATE TABLE GOALS (
  ID INT PRIMARY KEY NOT NULL,
  GOAL_MONTH INT NOT NULL,
  GOAL_YEAR INT NOT NULL,
  GOAL_TARGET INT NOT NULL,
  BILLABLE_DAYS INT NOT NULL
);
```

Some sample data should be added, similar to what is listed below:

```
INSERT INTO GOALS(ID, GOAL_MONTH, GOAL_YEAR, GOAL_TARGET, BILLABLE_DAYS) VALUES (1, 10, 2020, 250, 21);
INSERT INTO GOALS(ID, GOAL_MONTH, GOAL_YEAR, GOAL_TARGET, BILLABLE_DAYS) VALUES (2, 11, 2020, 250, 19);
INSERT INTO GOALS(ID, GOAL_MONTH, GOAL_YEAR, GOAL_TARGET, BILLABLE_DAYS) VALUES (3, 12, 2020, 250, 21);
```

The `GOALS` table, should now appear as shown below:

![Sample Goals Table](./GoalsTableQuery.png)


## Heroku Connect Configuration

Heroku Connect is an add-on which can be added to a Dyno to provide connectivity between Heroku and Salesforce.  For this project, the 
Demo Edition was selected to demonstrate the power of this add-on.

Once added to the Dyno in Heroku, a wizard will provide the necessary connectivity between Salesforce and Heroku PostgreSQL.  Once connectivity 
has been established, the `TimeEntry` (`TimeEntry__c`) object should be selected and the following fields should be chosen for synchronization:

* `date__c`
* `hours__c`
* `id`

For this project, a synchronization interval of 10 minutes will be fine - since time entries are only added once a day.  Below, 
is an example of the Mappings screen:

![Heroku Connect Mappings](./HerokuConnectMappings.png)

## RESTful URIs

Once all of the configuration items have been setup and the Dyno started with this repository code, the following URIs are available:

* `GET hostname/goal/{year}/{month}` returns a `Goal` entity which contains the following payload:

```
{
    "id": 1,
    "month": 10,
    "year": 2020,
    "goal": 250
}
```

* `GET hostname/time/{year}/{month}` returns a `List<Time>` result set which contains the following payload:

```
[
    {
        "id": 1,
        "date": "2020-10-01T04:00:00.000+00:00",
        "hours": 12.5
    },
    {
        "id": 2,
        "date": "2020-10-02T04:00:00.000+00:00",
        "hours": 10.0
    } ...
]
```
* `GET hostname/months` returns a `List<Month>` result set, which contains a list of valid month/year combination data:

```
[
    {
        "month": 10,
        "year": 2020,
        "displayName": "10/2020"
    } ...
]
```

Made with ♥ by johnjvester@gmail.com, because I enjoy writing code.